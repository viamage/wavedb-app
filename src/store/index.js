import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    lastPage: '/',
    searchTerm: ''
  },
  mutations: {
    setLastPage (state, payload) {
      state.lastPage = payload
    },
    setSearchTerm (state, payload) {
      state.searchTerm = payload
    }
  },
  actions: {
  }
})
