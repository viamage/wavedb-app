// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import 'bootstrap/dist/css/bootstrap.css'
import 'font-awesome/css/font-awesome.css'
import axios from 'axios'
import VueAxios from 'vue-axios'
import VueIdb from 'vue-idb'
import store from './store'
import './assets/js/swipe.js'

Vue.use(VueIdb)
Vue.use(VueAxios, axios)

Vue.config.devtools = true
Vue.config.productionTip = false

const idb = new VueIdb({
  version: 1,
  database: 'wavepath',
  schemas: [
    { practices: 'id, title, slug, system, type, favorite' }
  ]
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  idb: idb,
  components: { App },
  template: '<App/>'
})
