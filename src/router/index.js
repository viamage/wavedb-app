import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Practice from '@/components/Practice'
import Categories from '../components/Categories'
import Category from '../components/Category'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/practice/:slug',
      name: 'practice',
      component: Practice
    },
    {
      path: '/categories',
      name: 'categories',
      component: Categories
    },
    {
      path: '/category/:slug/:page?',
      name: 'category',
      component: Category
    }
  ]
})
